# README #

JPanelExpandable, is a swing component java. Is a group of panels that expanded and contracted for show more or minus information.

### What is this repository for? ###

* Quick summary
    - These repository have the code of JPanelExpandable that can be modified in Netbeans.
* Version
    - The current versión is V0.1 - 2016 / 04 / 26

### How do I get set up? ###

* Summary of set up:
    - You can download the JAR, [JPanelExpandable.jar](https://bitbucket.org/tutorials/markdowndemo).
* Configuration
    - __First__, You should be have a JFrame or JInternalFrame, that will be the parent.
    - __Second__, You should be have a Panel that will be the panel floating and this panel should inheritance of *info.equisd.JPanelExpandable.JPanelExpandableContent* (Class included in the project JPanelExpandable).
    - __Third__, In your JFrame or JInternalFrame, you must declare a variable and instance the class JPanelExpandable and add yours panels floating.

### Code examples ###
![Screen Shot 2016-04-28 at 11.43.39 AM.png](https://bitbucket.org/repo/aBRd9k/images/3160782596-Screen%20Shot%202016-04-28%20at%2011.43.39%20AM.png)

![Screen Shot 2016-04-28 at 11.45.14 AM.png](https://bitbucket.org/repo/aBRd9k/images/283197412-Screen%20Shot%202016-04-28%20at%2011.45.14%20AM.png)

![Screen Shot 2016-04-28 at 11.51.02 AM.png](https://bitbucket.org/repo/aBRd9k/images/2255379822-Screen%20Shot%202016-04-28%20at%2011.51.02%20AM.png)
![Screen Shot 2016-04-28 at 11.51.16 AM.png](https://bitbucket.org/repo/aBRd9k/images/2292458594-Screen%20Shot%202016-04-28%20at%2011.51.16%20AM.png)