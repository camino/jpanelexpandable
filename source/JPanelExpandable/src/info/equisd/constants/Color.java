/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.equisd.constants;

/**
 *
 * @author bichito
 */
public class Color {
    public static java.awt.Color TABLE_BACKGROUND_ALTERNATE = new java.awt.Color(242, 242, 242);
    public static java.awt.Color GREEN_DARK = java.awt.Color.decode("#008000");
    public static java.awt.Color GREEN_LIGHT = java.awt.Color.decode("#a8ffa8");
    public static java.awt.Color GRAY_LIGHT = java.awt.Color.decode("#aaaaaa");
    public static java.awt.Color GRAY_LIGHT_DK = java.awt.Color.decode("#777777");
    public static java.awt.Color BLUE_LIGHT = java.awt.Color.decode("#4da6ff");
}
