/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.equisd.components;

import static info.equisd.constants.Color.BLUE_LIGHT;
import info.equisd.utils.Picturize;

/**
 *
 * @author bichito
 */
public class JPanelExpandable {

    /**
     * Specific the parent of expandable.
     */
    protected java.awt.Component frame;

    /**
     * Specific a object that was selected for expandable.
     */
    protected Object itemCurrent;

    /**
     * Specific the panel where will draw the expandable.
     */
    protected javax.swing.JPanel area;

    /**
     * Specific the panel where will draw the panels.
     */
    protected javax.swing.JPanel wrapContent;

    /**
     * Specific the manager layout of panels.
     */
    protected java.awt.CardLayout managerContent;

    /**
     * 
     */
    protected boolean isActive = false;

    /**
     * Specific if the expandable is active or not.
     */
    protected java.util.ArrayList<JPanelExpandableAction> actionList;

    /**
     * Specific the panel that is current.
     */
    protected JPanelExpandableAction actionCurrent;

    /**
     *
     */
    protected JPanelExpandableListener listener;

    /**
     *
     */
    final protected int WIDTH_ACTION = 33;

    /**
     *
     */
    final protected int WIDTH_LEFT = 0;

    /**
     *
     */
    final protected int HEIGHT_LEFT = 10;

    /**
     *
     */
    final protected int WIDTH_R_ACTION = 45;

    /**
     *
     */
    final protected int HEIGHT_R_ACTION = 1;

    /**
     *
     */
    final protected int WIDTH_LEFT_STARTED_OF_RIGHT = 42;

    /**
     *
     */
    protected boolean status = false;

    /**
     *
     */
    protected java.awt.ComponentOrientation ORIENTATION = java.awt.ComponentOrientation.LEFT_TO_RIGHT;
    protected int startX = -99,

    /**
     *
     */
    startY = -99;

    /**
     *
     * @param values
     */
    public void setStartY(int values) {
	this.startY = values;
    }

    /**
     *
     * @return
     */
    public int getStartY() {
	return this.startY == -99?this.HEIGHT_LEFT: this.startY;
    }

    /**
     *
     * @param values
     */
    public void setStartX(int values) {
	this.startX = values;
    }

    /**
     *
     * @return
     */
    public int getStartX() {
	return this.startX == -99?this.WIDTH_LEFT: this.startX;
    }       

    /**
     *
     * @param panelId
     */
    public void setContent(String panelId) {
	this.managerContent.show(this.wrapContent, panelId);
	for (JPanelExpandableAction obj : actionList) {
	    if (obj.getId().equals(panelId)) {
		this.actionCurrent = obj;
		break;
	    }
	}
    }

    /**
     *
     * @param value
     */
    public void setListener(JPanelExpandableListener value) {
	this.listener = value;
    }

    /**
     *
     * @return
     */
    public JPanelExpandableListener getListener() {
	return this.listener;
    }

    /**
     *
     * @return
     */
    public JPanelExpandableAction getContent() {
	return this.actionCurrent;
    }

    /**
     *
     * @param ORIENTATION
     */
    public void setOrientation(java.awt.ComponentOrientation ORIENTATION) {
	this.ORIENTATION = ORIENTATION;
    }
    private void closeExcept(JPanelExpandableAction panelAction) {
	for (JPanelExpandableAction obj : actionList) {
	    if (!obj.getId().equals(panelAction.getId())) {
		obj.onClose(true);
	    }
	}
    }
    private void onOpenDefault() {
	if (actionList.size() > 0) {
	    actionList.get(0).onOpen();
	}
    }
    private void onCloseAll() {
	for (JPanelExpandableAction obj : actionList) {
	    obj.onClose(true);
	}
    }

    /**
     *
     */
    public static class JPanelExpandableAction {

	/**
	 *
	 */
	public String title;

	/**
	 *
	 */
	public javax.swing.JPanel action;

	/**
	 *
	 */
	public javax.swing.JSeparator separator;

	/**
	 *
	 */
	public JPanelExpandableContent expandable;

	/**
	 *
	 */
	protected JPanelExpandable parent;

	/**
	 *
	 */
	protected JPanelExpandableListener listener;

	/**
	 *
	 */
	protected javax.swing.JLabel iconLabel;

	/**
	 *
	 */
	final protected String ICON_ON = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABx0lEQVQ4T4WTQXLaQBREu8UoylI5QaDKsLUSex+4AZwgeBW04wbmCOxkZwO+gXMCcwCI8RZchXKCsAylkX7qSxYlME5mNzX8R//uFnF0gsnGr+2SrwS7ANr6LJAYwIzCu3nYnFVHWL1c3D53ITKh4CmDTB0wtjXZmpS+UIHSJ/BoPdNbXjW2OrsHfI5WfZJjERn+DFvTY2V6z9X9ScYkzq3ndhSSA4JoU6/RPoK8Wnw7uz81fKA0Wk1JfpwPmp0ccBGt8n9chK1++cPCCzsB+GsxOBtWAS9vMQVd6sXs7G8rprEMG2pWLtXskgcBmHpuu9z3QMXN8xiS+byM1m2hjBeDVnA4zDqBPh3kZpUncc2TAl/mJlTnmclwHjbzyC6j9QzElzd9EHQ0SgWAeMgVABiVgOD7KqhlnAESU/jKUAsz1VX3AE3A0G6sZz6Uu5aQf0WqsTug9kJTWMcCGVXz/x9EV80c3OeAokS4Tj330ynHj/0oS5d6Ro0ujnah2rC3TKzUvadm7gFlTfME6IzSd7UfVTVFW5NrgL2qNwcfU2WdIcFz/QILJeIDDETkLoU7KgunL68A+yprOrB1cegzk619b5an/PkLG6r/Rd0uWukAAAAASUVORK5CYII=";

	/**
	 *
	 */
	final protected String ICON_OFF = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABy0lEQVQ4T3VTS3LaQBTs1ifyLsoJAlWBrZXgffAN4ASBVaxVuIE5gnZyvAHfgJzAHABivBWughuEpSlGeqkZEDXIeHZPo9fTr7sfUTnRaBW6290Pgh0AbX0tkDWAKYUPs7gxtVtoF63fLx2IjCh4LiBjB1wrVzZezlCoAaVH4EkFXnfRr2907xHgW5r1SCYiMvgbN8dVZro27F53CYlLFfjXGsQAROmq5lI9gezPf36ZnGs+YZpmY5KfZzeNawPQSjPz4jxu9uwf93qosYhMbFaH72sKOtSFt1X/lHj1RVzXYp2c6D6L3ILT6mitu5cEUoS8SpdtoSTzm2ZkxrnPIh/8aKMUhUQEk0KkXzI59I2olWchg1ncMJZdpcspiO/v6SBkV+ukAUA8GgYAhiWAdsMBazaAkCEhvwTynAd+W6t/BNAOeFQrFXifSm+rQnrb3aMALJv1/eEhnQvtwnItkGHV/73Ab5vLUQsHEwOwDxFu88D/eo5FVY8ydHng1Y5J1FmwE/aeiFbcu3ovjgBlTI0DdIb5B/ePzWaf1t0twK6diZNlssYZELzUG7hnIiHASEQecvhDO3BvAErqxh2omjgMWchGXXiLc/r8BybnAlSuurX5AAAAAElFTkSuQmCC";

	/**
	 *
	 */
	final protected String FIRST_ID = "panel-1";

	/**
	 *
	 */
	protected String id;
	private boolean isActive;
	private boolean enabled;
	protected boolean already = false;
	
	/**
	 *
	 * @param title
	 * @param expandable
	 * @param parent
	 * @param id
	 */
	public JPanelExpandableAction(String title, JPanelExpandableContent expandable, JPanelExpandable parent, String id) {
	    this.id = id;
	    this.title = title;
	    this.action = new javax.swing.JPanel();
	    this.expandable = expandable;
	    this.parent = parent;
	    this.enabled = true;
	    this.separator = new javax.swing.JSeparator();
	    this.separator.setBorder(javax.swing.BorderFactory.createLineBorder(BLUE_LIGHT));
	    
	    iconLabel = new javax.swing.JLabel();
	    Picturize picture = new Picturize(ICON_OFF);
	    iconLabel.setIcon(new javax.swing.ImageIcon(picture.image)); // NOI18N
	    iconLabel.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
	    iconLabel.setText(String.format("<html>%s</html>", this.getTitleFormat()));
	    iconLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
	    iconLabel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

	    this.action.setBackground(new java.awt.Color(255, 255, 255));
	    this.action.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 3, 0, 0, java.awt.Color.WHITE));
	    
	    this.action.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

	    javax.swing.GroupLayout panelActionLayout = new javax.swing.GroupLayout(this.action);
	    this.action.setLayout(panelActionLayout);
	    panelActionLayout.setHorizontalGroup(
		    panelActionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		    .addGroup(panelActionLayout.createSequentialGroup()
			    .addContainerGap()
			    .addComponent(iconLabel)
			    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	    );
	    panelActionLayout.setVerticalGroup(
		    panelActionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		    .addGroup(panelActionLayout.createSequentialGroup()
			    .addContainerGap()
			    .addComponent(iconLabel)
			    .addContainerGap(0, Short.MAX_VALUE))
	    );

	    this.action.addMouseListener(new java.awt.event.MouseListener() {
		@Override
		public void mouseClicked(java.awt.event.MouseEvent e) {
		}

		@Override
		public void mousePressed(java.awt.event.MouseEvent e) {
		}

		@Override
		public void mouseReleased(java.awt.event.MouseEvent e) {
		    if (JPanelExpandableAction.this.enabled) {
			if (!JPanelExpandableAction.this.isActive) {
			    JPanelExpandableAction.this.onOpen();
			} else {
			    JPanelExpandableAction.this.onClose(false);
			}
		    } else {
			JPanelExpandableAction.this.onClose(false);
		    }

		}

		@Override
		public void mouseEntered(java.awt.event.MouseEvent e) {
		    if (!JPanelExpandableAction.this.isActive && JPanelExpandableAction.this.enabled) {
			JPanelExpandableAction.this.action.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 3, 0, 0, BLUE_LIGHT));
		    }
		}

		@Override
		public void mouseExited(java.awt.event.MouseEvent e) {
		    if (!JPanelExpandableAction.this.isActive && JPanelExpandableAction.this.enabled) {
			JPanelExpandableAction.this.action.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 3, 0, 0, java.awt.Color.WHITE));
		    }
		}
	    });

	}
	
	/**
	 *
	 * @return
	 */
	public boolean alreadyOpenend() {
	    return already;
	}
	
	private String getTitleFormat() {
	    String newStr = "";
	    for (int i = 0; i < title.length(); i++) {
		newStr += title.charAt(i) + "<br>";
	    }
	    return newStr;
	}

	/**
	 *
	 * @return
	 */
	public String getId() {
	    return id;
	}

	/**
	 *
	 * @return
	 */
	public boolean isFirst() {
	    return this.id.equals(FIRST_ID);
	}

	/**
	 *
	 */
	public void onOpen() {
	    this.parent.closeExcept(this);
	    this.parent.setContent(this.getId());
	    this.parent.onOpen();
	    Picturize picture = new Picturize(ICON_ON);
	    iconLabel.setIcon(new javax.swing.ImageIcon(picture.image)); // NOI18N		
	    this.action.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 3, 0, 0, BLUE_LIGHT));
	    this.isActive = true;
	    this.already = true;
	}

	/**
	 *
	 * @param simulated
	 */
	public void onClose(boolean simulated) {
	    this.parent.onClose();
	    Picturize picture = new Picturize(ICON_OFF);
	    iconLabel.setIcon(new javax.swing.ImageIcon(picture.image)); // NOI18N	
	    if ( !simulated ) {
		this.action.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 3, 0, 0, BLUE_LIGHT));
	    } else {
		this.action.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 3, 0, 0, java.awt.Color.WHITE));
	    }
	    this.isActive = false;
	}

	/**
	 *
	 * @param listener
	 */
	public void setListener(JPanelExpandableListener listener) {
	    this.listener = listener;
	}

	/**
	 *
	 * @return
	 */
	public JPanelExpandableListener getListener() {
	    return this.listener;
	}	
	
	/**
	 *
	 * @param value
	 */
	public void setEnabled(boolean value) {
	    enabled = value;
	    if (value) {
		this.action.setBackground(java.awt.Color.WHITE);
		this.action.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
	    } else {
		this.action.setBackground(java.awt.Color.LIGHT_GRAY);
		this.action.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	    }
	}
    }

    /**
     *
     */
    public static class JPanelExpandableListener {

	/**
	 *
	 * @param content
	 */
	public void onOpen(JPanelExpandableAction content) {
	}

	/**
	 *
	 * @param content
	 */
	public void onClose(JPanelExpandableAction content) {
	}
    }

    /**
     *
     */
    public static class JPanelExpandableContent extends javax.swing.JPanel {

	/**
	 *
	 */
	protected JPanelExpandableAction parent;

	/**
	 *
	 */
	public void onClose() {
	    this.parent.onClose(true);
	}

	private void setParent(JPanelExpandableAction value) {
	    this.parent = value;
	}
    }

//    /**
//     *
//     * @return
//     */
//    public Object getItemSelected() {
//	return itemCurrent;
//    }
//
//    /**
//     *
//     * @param value
//     */
//    public void setItemSelected(Object value) {
//	itemCurrent = value;
//    }

    /**
     *
     * @return
     */
    public java.awt.Component getFrame() {
	return this.frame;
    }

    /**
     *
     */
    public JPanelExpandable() {
	super();
	actionList = new java.util.ArrayList<>();
    }

    /**
     *
     * @param frame
     */
    public void setFrame(java.awt.Component frame) {
	this.frame = frame;
    }

    /**
     *
     * @param values
     */
    public void setActionList(java.util.ArrayList<JPanelExpandableAction> values) {
	this.actionList = values;
    }

    /**
     *
     * @return
     */
    public java.util.ArrayList<JPanelExpandableAction> getActionList() {
	return this.actionList;
    }

    private javax.swing.GroupLayout.SequentialGroup drawHorizontal(javax.swing.GroupLayout layout) {
	javax.swing.GroupLayout.ParallelGroup parallel = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);

	for (JPanelExpandableAction obj : actionList) {
	    if ( obj.isFirst() ) {
		parallel
		    .addComponent(obj.action, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE);
	    } else {
		parallel
		    .addComponent(obj.separator, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
		    .addComponent(obj.action, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE);
	    }

	}

	javax.swing.GroupLayout.SequentialGroup sequential = layout.createSequentialGroup()
		.addGap(0, 0, 0)
		.addGroup(parallel)
		.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
		.addComponent(wrapContent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		.addGap(0, 0, 0);

	return sequential;
    }

    private javax.swing.GroupLayout.SequentialGroup drawVertical(javax.swing.GroupLayout layout) {
	javax.swing.GroupLayout.SequentialGroup sequentialRoot = layout.createSequentialGroup();
	javax.swing.GroupLayout.SequentialGroup sequentialActions = layout.createSequentialGroup();
	for (int i = 0; actionList.size() > i; i++) {
	    if (i == 0) {
		sequentialActions
			.addComponent(actionList.get(i).action, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
	    } else {
		sequentialActions
			.addGap(0, 0, 0)
			.addComponent(actionList.get(i).separator, javax.swing.GroupLayout.PREFERRED_SIZE, 1, javax.swing.GroupLayout.PREFERRED_SIZE)
			.addGap(0, 0, 0)
			.addComponent(actionList.get(i).action, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
	    }
	}

	javax.swing.GroupLayout.ParallelGroup parallel = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
	parallel.addComponent(wrapContent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
	parallel.addGroup(sequentialActions);

	sequentialRoot.addGap(0, 0, 0)
		.addGroup(parallel)
		.addGap(0, 0, 0);

	return sequentialRoot;
    }

    /**
     *
     */
    public void run() {
	if (this.actionList.size() <= 0) {
	    this.addPanel("Tab1", new JPanelExpandableContent());
	}
	this.managerContent = new java.awt.CardLayout();
	this.wrapContent = new javax.swing.JPanel();
	this.area = new javax.swing.JPanel();
	this.area.setComponentOrientation(ORIENTATION);		

	this.wrapContent.setLayout(managerContent);

	if (ORIENTATION.equals(java.awt.ComponentOrientation.LEFT_TO_RIGHT)) {
	    this.area.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 1, 1, BLUE_LIGHT));
	} else {
	    this.area.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 0, 0, BLUE_LIGHT));
	}
	javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.area);
	this.area.setLayout(layout);

	layout.setHorizontalGroup(
		layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		.addGroup(drawHorizontal(layout)));

	layout.setVerticalGroup(
		layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		.addGroup(drawVertical(layout)));
	this.area.setBackground(java.awt.Color.WHITE);

	for (JPanelExpandableAction obj : actionList) {
	    this.wrapContent.add(obj.expandable, obj.getId());
	}

	this.frame.setFocusable(true);
	this.frame.addKeyListener(new java.awt.event.KeyListener() {
	    @Override
	    public void keyTyped(java.awt.event.KeyEvent e) {
	    }

	    @Override
	    public void keyPressed(java.awt.event.KeyEvent e) {
	    }

	    @Override
	    public void keyReleased(java.awt.event.KeyEvent e) {
		if (java.awt.event.KeyEvent.VK_F6 == e.getKeyCode()) {
		    if (!isActive) {
			JPanelExpandable.this.onOpenDefault();
			//JInternalPanelExpandable.this.onOpen();
		    }
		} else if (java.awt.event.KeyEvent.VK_ESCAPE == e.getKeyCode()) {
		    if (isActive) {
			JPanelExpandable.this.onCloseAll();
		    }
		}

	    }
	});
	javax.swing.JLayeredPane layeredPane = getLayeredPane();

	if (ORIENTATION.equals(java.awt.ComponentOrientation.LEFT_TO_RIGHT)) {
	    setWidth(0);
	} else {
	    // this.iframe.getPreferredSize().width - WIDTH_LEFT_STARTED_OF_RIGHT => Hasta el extremo derecho del panel
	    // WIDTH_ACTION => El ancho del area clicable
	    this.area.setBounds(this.frame.getPreferredSize().width - WIDTH_LEFT_STARTED_OF_RIGHT, this.getStartY(), WIDTH_ACTION + 500, this.area.getPreferredSize().height + HEIGHT_R_ACTION);
	}
	if (layeredPane != null) {
	    layeredPane.add(this.area, javax.swing.JLayeredPane.POPUP_LAYER);
	}
	this.area.setFocusable(true);
	status = true;

    }

    private void setWidth(int width) {
	this.area.setBounds(this.getStartX(), this.getStartY(), WIDTH_ACTION + width, this.area.getPreferredSize().height + HEIGHT_R_ACTION);
    }
    
    /**
     *
     * @return
     */
    public javax.swing.JLayeredPane getLayeredPane() {
	javax.swing.JLayeredPane layeredPane = null;
	if (this.frame instanceof javax.swing.JFrame) {
	    javax.swing.JFrame jFrame = (javax.swing.JFrame) this.frame;
	    layeredPane = jFrame.getLayeredPane();
	} else if (this.frame instanceof javax.swing.JInternalFrame) {
	    javax.swing.JInternalFrame jInternalFrame = (javax.swing.JInternalFrame) this.frame;
	    layeredPane = jInternalFrame.getLayeredPane();
	}
	return layeredPane;
    }

    /**
     *
     */
    public void onOpen() {
	if (!status) {
	    return;
	}
	if (this.area.getParent() instanceof javax.swing.JLayeredPane) {
	    // <editor-fold defaultstate="collapsed" desc="Resize panel">
	    if (ORIENTATION.equals(java.awt.ComponentOrientation.LEFT_TO_RIGHT)) {
		this.setWidth(this.area.getPreferredSize().width - WIDTH_ACTION);
	    } else if (this.frame.getPreferredSize().width > this.area.getPreferredSize().width) {
		this.area.setBounds((this.frame.getPreferredSize().width - (this.area.getPreferredSize().width + WIDTH_LEFT_STARTED_OF_RIGHT)),
			this.getStartY(),
			this.area.getPreferredSize().width + WIDTH_ACTION,
			this.area.getPreferredSize().height + HEIGHT_R_ACTION);
	    } else {
		this.area.setBounds((this.frame.getPreferredSize().width - (this.area.getPreferredSize().width + WIDTH_LEFT_STARTED_OF_RIGHT)),
			this.getStartY(),
			this.area.getPreferredSize().width + WIDTH_ACTION,
			this.area.getPreferredSize().height + HEIGHT_R_ACTION);
	    }
	    // </editor-fold>
	    isActive = true;
	    if (this.listener != null) {
		this.listener.onOpen(this.getContent());
	    }
	    System.out.println(this.area.getParent().getClass().getName());
	    
	    ((javax.swing.JLayeredPane) this.area.getParent()).moveToFront(this.area);
	    boolean flag = false;
	    for (java.awt.Component obj : this.area.getComponents()) {
		if (obj instanceof javax.swing.JTextField) {
		    if (!flag) {
			obj.requestFocus();
			firingOnElement(obj);
			break;
		    }
		}
	    }
	}
    }

    private void firingOnElement(java.awt.Component element) {
	element.addKeyListener(new java.awt.event.KeyListener() {
	    @Override
	    public void keyTyped(java.awt.event.KeyEvent e) {

	    }

	    @Override
	    public void keyPressed(java.awt.event.KeyEvent e) {

	    }

	    @Override
	    public void keyReleased(java.awt.event.KeyEvent e) {
		if (java.awt.event.KeyEvent.VK_ESCAPE == e.getKeyCode()) {
		    JPanelExpandable.this.onClose();
		}
	    }

	});
    }

    /**
     *
     */
    public void onClose() {
	if (!status) {
	    return;
	}
	if (ORIENTATION.equals(java.awt.ComponentOrientation.LEFT_TO_RIGHT)) {
	    setWidth(0);
	} else {
	    this.area.setBounds(this.frame.getPreferredSize().width - WIDTH_LEFT_STARTED_OF_RIGHT, this.getStartY(), WIDTH_ACTION, this.area.getPreferredSize().height + HEIGHT_R_ACTION);
	}

	isActive = false;
	if (this.listener != null) {
	    this.listener.onClose(this.getContent());
	    this.frame.requestFocus();
	}
    }

    /**
     *
     */
    public void stop() {
	status = false;
    }

    /**
     *
     */
    public void reRun() {
	status = true;
    }

    /**
     *
     * @param title
     * @param panel
     * @return
     */
    public JPanelExpandableAction addPanel(String title, JPanelExpandableContent panel) {
	if (actionList != null) {
	    int nPanels = actionList.size();
	    JPanelExpandableAction tmp = new JPanelExpandableAction(title, panel, this, "panel-" + (nPanels + 1));
	    panel.setParent(tmp);
	    actionList.add(tmp);
	    return tmp;
	}
	return null;
    }
}
