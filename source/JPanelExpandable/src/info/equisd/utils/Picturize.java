/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.equisd.utils;


/**
 *
 * @author bichito
 */
public class Picturize {
        public int height;
    public int width;
    public java.awt.Image image;
    public Picturize(java.io.File source) throws java.io.IOException {
	image = javax.imageio.ImageIO.read(source);
	java.awt.image.BufferedImage bufferedImage = (java.awt.image.BufferedImage)image;
	height = bufferedImage.getHeight();
	width = bufferedImage.getWidth();
    }
    public Picturize(String str) {
        try (java.io.ByteArrayInputStream bis = new java.io.ByteArrayInputStream(java.util.Base64.getDecoder().decode(str))) {  
            image = javax.imageio.ImageIO.read(bis);  
        } catch (Exception e) {  
        }  
    }

}
